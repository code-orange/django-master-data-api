from django.urls import path, include
from tastypie.api import Api

from django_master_data_api.django_master_data_api.api.resources import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.api.resources import *

v1_api = Api(api_name="v1")

# API GENERAL
v1_api.register(ApiAuthCustomerApiKeyResource())
v1_api.register(ApiAuthCustomerResource())

# API MDAT
v1_api.register(MdatCustomersResource())
v1_api.register(MdatItemsResource())
v1_api.register(MdatItemsDatasheetResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
