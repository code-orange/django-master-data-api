from base64 import b64encode

import requests
from django.forms import model_to_dict
from tastypie.exceptions import BadRequest
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_mdat_customer.django_mdat_customer.models import *
from django_mdat_prod_info_mgmt.django_mdat_prod_info_mgmt.models import (
    MdatItemDataSheets,
)
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.authentification import (
    ApiAuthCustomerAuthentication,
)
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.models import (
    ApiAuthCustomerApiKey,
)


def check_permission(tenant: str, customer_id: int):
    user_api_auth = ApiAuthCustomerApiKey.objects.get(user_id=customer_id)

    if user_api_auth.super_admin:
        return True

    return False


class MasterDataBeBaseResource(Resource):
    class Meta:
        queryset = None
        always_return_data = True
        allowed_methods = ["get", "post"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()
        limit = 0
        max_limit = 0


class MdatCustomersResource(MasterDataBeBaseResource):
    class Meta(MasterDataBeBaseResource.Meta):
        queryset = MdatCustomers.objects.all()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        if not check_permission("", bundle.request.user.id):
            raise BadRequest("Wrong tenant.")

        return MdatCustomers.objects.all()

    def obj_get(self, bundle, **kwargs):
        try:
            customer = MdatCustomers.objects.get(id=kwargs["pk"])
        except MdatCustomers.DoesNotExist:
            raise BadRequest("Wrong tenant.")

        if not check_permission(customer.org_tag, bundle.request.user.id):
            raise BadRequest("Wrong tenant.")

        return customer

    def obj_create(self, bundle, **kwargs):
        me = bundle.request.user

        try:
            customer = me.get_reseller_customers().get(
                external_id=bundle.data["external_id"]
            )
        except MdatCustomers.DoesNotExist:
            customer = MdatCustomers()

        customer.created_by = me
        customer.name = bundle.data["name"]
        customer.external_id = bundle.data["external_id"]

        if "enabled" in bundle.data:
            customer.enabled = bundle.data["enabled"]
        else:
            customer.enabled = True

        if "primary_email" in bundle.data:
            customer.primary_email = bundle.data["primary_email"]

        if "billing_email" in bundle.data:
            customer.billing_email = bundle.data["billing_email"]

        if "tech_email" in bundle.data:
            customer.tech_email = bundle.data["tech_email"]

        customer.save()

        bundle.obj = customer

        return bundle

    def dehydrate(self, bundle):
        bundle.data = model_to_dict(bundle.obj)
        bundle.data["delivery_address"] = (
            bundle.obj.mdatcustomerdeliveryaddresses.address.to_dict()
        )
        bundle.data["billing_address"] = (
            bundle.obj.mdatcustomerbillingaddresses.address.to_dict()
        )

        # TODO: make company_logo work
        if "company_logo" in bundle.data:
            del bundle.data["company_logo"]

        return bundle


class MdatItemsResource(MasterDataBeBaseResource):
    class Meta(MasterDataBeBaseResource.Meta):
        queryset = MdatItems.objects.all()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        if not check_permission("", bundle.request.user.id):
            raise BadRequest("Wrong tenant.")

        return MdatItems.objects.all()

    def obj_get(self, bundle, **kwargs):
        try:
            item = MdatItems.objects.get(id=kwargs["pk"])
        except MdatItems.DoesNotExist:
            raise BadRequest("Wrong tenant.")

        if not check_permission(item.created_by.org_tag, bundle.request.user.id):
            raise BadRequest("Wrong tenant.")

        return item

    def obj_create(self, bundle, **kwargs):
        return bundle

    def dehydrate(self, bundle):
        bundle.data = model_to_dict(bundle.obj)
        return bundle


class MdatItemsDatasheetResource(MasterDataBeBaseResource):
    class Meta(MasterDataBeBaseResource.Meta):
        queryset = MdatItemDataSheets.objects.all()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        if not check_permission("", bundle.request.user.id):
            raise BadRequest("Wrong tenant.")

        return MdatItemDataSheets.objects.all()

    def obj_get(self, bundle, **kwargs):
        try:
            item = MdatItems.objects.get(id=kwargs["pk"])
        except MdatItems.DoesNotExist:
            raise BadRequest("Wrong item.")

        if not check_permission(item.created_by.org_tag, bundle.request.user.id):
            raise BadRequest("Wrong tenant.")

        itscope_datasheets = [
            "manufacturerDatasheet",
            "standardPdfDatasheet",
        ]

        datasheet_content = None

        for itscope_datasheet in itscope_datasheets:
            try:
                datasheet = MdatItemDataSheets.objects.get(item_id=item.id)
            except MdatItemDataSheets.DoesNotExist:
                if itscope_datasheet in item.mdatitemcacheitscope.content:
                    if item.mdatitemcacheitscope.content[itscope_datasheet] != "":
                        datasheet_request = requests.get(
                            item.mdatitemcacheitscope.content[itscope_datasheet]
                        )

                        if datasheet_request.status_code == 200:
                            datasheet_content = datasheet_request.content
                            break
            else:
                datasheet_content = datasheet.content

        if datasheet_content is None:
            raise BadRequest("No datasheet available.")

        return {"pdf": b64encode(datasheet_content)}

    def obj_create(self, bundle, **kwargs):
        return bundle

    def dehydrate(self, bundle):
        bundle.data = bundle.obj
        return bundle
